import React from "react"
import PokeCard from "./PokeCard"

export default {
  title: "PokeCard",
  component: PokeCard,
}
const Template = args => <PokeCard {...args} />

export const Render = Template.bind({})
Render.args = {
  pokemon: {
    id: 25,
    name: "pikachu",
    sprites: {
      other: {
        dream_world: {
          front_default:
            "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/25.svg",
        },
      },
    },
  },
}
export const ImageNotFound = Template.bind({})
ImageNotFound.args = {
  ...Render.args,
  pokemon: {
    ...Render.args.pokemon,
    sprites: null,
  },
}
