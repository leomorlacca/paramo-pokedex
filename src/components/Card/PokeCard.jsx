import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
} from "@mui/material"
import { Link as RouterLink } from "react-router-dom"
import NotFound from "../../assets/image-404.png"
import "./PokeCard.scss"

const PokeCard = ({ pokemon }) => {
  const image = pokemon.sprites?.other.dream_world.front_default || NotFound
  return (
    <Card sx={{ maxWidth: 280 }} className="card">
      <CardActionArea component={RouterLink} to={`/pokedetail/${pokemon.id}`}>
        <CardMedia
          className="card-media"
          component="img"
          height="280"
          image={image}
          alt={pokemon.name}
        />
        <CardContent className="card-content">
          <Typography variant="h5" textAlign="center" component="div">
            <span className="poke-number">#{pokemon.id} -</span>{" "}
            <span className="poke-name">{pokemon.name}</span>
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}

export default PokeCard
