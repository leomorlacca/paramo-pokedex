import "./Footer.scss"

const Footer = () => {
  return (
    <div className="poke-footer">
      <span>
        © 2022 - Leonardo Morlacca Pokédex for Paramo - All Rights Reserved
      </span>
    </div>
  )
}
export default Footer
