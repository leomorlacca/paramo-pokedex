import React from "react"
import Footer from "./Footer"
import "./Footer.scss"

export default {
  title: "Footer",
  component: Footer,
}

const Template = args => <Footer {...args} />

export const Render = Template.bind({})
