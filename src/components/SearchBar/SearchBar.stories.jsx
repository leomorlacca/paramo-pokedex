import React from "react"
import SearchBar from "./SearchBar"

export default {
  title: "SearchBar",
  component: SearchBar,
}
const pokelist = [{ id: 1, name: "pikachu" }]
const Template = args => <SearchBar {...args} />

export const Render = Template.bind({})
Render.args = {
  pokelist: pokelist,
}
