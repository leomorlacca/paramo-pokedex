import {
  Autocomplete,
  TextField,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  Grid,
} from "@mui/material"
import React, { useState } from "react"

const SearchBar = ({
  pokelist,
  getSearchResult,
  getSortValue,
  getPageSize,
  pageSize,
  setPageSize,
  pokemonName,
  setPokemonName,
}) => {
  const [sortValue, setSortValue] = useState("id-asc")

  const handleSearchPokemon = result => {
    setPokemonName(result)
    getSearchResult(result)
  }

  const handleSortPokemon = e => {
    setSortValue(e.target.value)
    getSortValue(e.target.value)
  }

  const handleChangePageSize = e => {
    setPageSize(e.target.value)
    getPageSize(e.target.value)
  }

  return (
    <Grid container spacing={2} paddingX={1}>
      <Grid item xs={12} md={8}>
        <Autocomplete
          className="search-bar"
          fullWidth
          clearOnBlur={false}
          isOptionEqualToValue={(option, value) => option.value === value.value}
          value={pokemonName}
          onChange={(event, newValue) => handleSearchPokemon(newValue)}
          id="poke-search"
          options={pokelist}
          getOptionLabel={option => option.name || ""}
          renderInput={params => (
            <TextField
              {...params}
              label={`Search over ${pokelist.length} Pokémon...`}
              InputProps={{
                ...params.InputProps,
                type: "search",
              }}
            />
          )}
        />
      </Grid>
      <Grid item xs={12} md={2}>
        <FormControl fullWidth>
          <InputLabel id="sort-label">Sort by</InputLabel>
          <Select
            value={sortValue}
            label="Order"
            labelId="sort-label"
            id="sort"
            onChange={handleSortPokemon}>
            <MenuItem value={"id-asc"}>ID asc</MenuItem>
            <MenuItem value={"id-desc"}>ID desc</MenuItem>
            <MenuItem value={"name-asc"}>Name asc</MenuItem>
            <MenuItem value={"name-desc"}>Name desc</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12} md={2}>
        <FormControl fullWidth>
          <InputLabel id="size-label">Items per page</InputLabel>
          <Select
            value={pageSize}
            label="Size"
            labelId="size-label"
            id="size"
            onChange={handleChangePageSize}>
            <MenuItem value={60}>60</MenuItem>
            <MenuItem value={120}>120</MenuItem>
            <MenuItem value={180}>180</MenuItem>
            <MenuItem value={240}>240</MenuItem>
          </Select>
        </FormControl>
      </Grid>
    </Grid>
  )
}

export default SearchBar
