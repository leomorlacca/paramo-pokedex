import "./Header.scss"
import { Link as RouterLink } from "react-router-dom"

const Header = () => {
  return (
    <div className="header">
      <RouterLink to={"/"}>
        <img
          className="header-logo"
          alt="Pokemon logo"
          src={require("../../assets/pokelogo.png")}></img>
      </RouterLink>
      <h1>Pokédex</h1>
    </div>
  )
}

export default Header
