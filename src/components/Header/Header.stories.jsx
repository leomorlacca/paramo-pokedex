import React from "react"
import Header from "./Header"
import "./Header.scss"

export default {
  title: "Header",
  component: Header,
}

const Template = args => <Header {...args} />

export const Render = Template.bind({})
