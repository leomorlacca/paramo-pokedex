import React from "react"
import StatBar from "./StatBar"

export default {
  title: "StatBar",
  component: StatBar,
}

const Template = args => <StatBar {...args} />

export const Render = Template.bind({})
Render.args = {
  label: "Special-Attack",
  value: 56,
}
