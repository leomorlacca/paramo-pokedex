import { Box, Typography, LinearProgress } from "@mui/material"

const StatBar = ({ label, value }) => {
  return (
    <Box marginBottom={3}>
      <Typography textTransform="capitalize">
        {label} - {value}%
      </Typography>
      <LinearProgress
        color="secondary"
        className="progress-bar"
        variant="determinate"
        value={value}
      />
    </Box>
  )
}
export default StatBar
