import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client"
import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import PokeList from "./pages/PokeList/PokeList"
import PokeDetail from "./pages/PokeDetail/PokeDetail"
import Header from "./components/Header/Header"
import Footer from "./components/Footer/Footer"

const client = new ApolloClient({
  uri: "https://beta.pokeapi.co/graphql/v1beta",
  cache: new InMemoryCache(),
})

function App() {
  return (
    <ApolloProvider client={client}>
      <Router>
        <Header />
        <Routes>
          <Route strict exact path="/" element={<PokeList />} />
          <Route strict exact path="/pokedetail/:id" element={<PokeDetail />} />
        </Routes>
        <Footer />
      </Router>
    </ApolloProvider>
  )
}

export default App
