import { gql, useQuery } from "@apollo/client"

// I wanted to use GraphQL in the entire pokedex but since it's a new tool for me (and for time reasons),
// I only used it for getting the Pokemon Detail, which is easier. Btw, very interesting tool to learn more about.
const usePokeDetail = id => {
  const GET_POKEMON_DETAIL = gql`
  query GetPokemons {
    pokemon_v2_pokemon(where: { id: { _eq: ${id} } }) {
      name
      id
      base_experience
      order
      weight
      height
      pokemon_v2_pokemonsprites {
          sprites
      }
      pokemon_v2_pokemonabilities {
        pokemon_v2_ability {
          name
        }
      }
      pokemon_v2_pokemontypes {
        pokemon_v2_type {
          name
        }
      }
      pokemon_v2_pokemonspecy {
        capture_rate
        base_happiness
      }
      pokemon_v2_pokemonstats {
        base_stat
        pokemon_v2_stat {
          name
        }
      }
    }
    pokemon_v2_pokemonhabitat_by_pk(id: ${id}) {
      name
    }
    pokemon_v2_pokemonshape_by_pk(id: ${id}) {
      name
    }
    pokemon_v2_region_by_pk(id: ${id}) {
      name
    }
  }
`

  const { loading, error, data } = useQuery(GET_POKEMON_DETAIL)

  return {
    loading,
    error,
    data,
  }
}

export default usePokeDetail
