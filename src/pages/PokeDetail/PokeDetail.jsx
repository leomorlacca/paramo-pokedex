import { useParams, useNavigate } from "react-router-dom"
import { Button, Box, Typography } from "@mui/material"
import StatBar from "../../components/StatBar/StatBar"
import usePokeDetail from "../../hooks/usePokeDetail"
import NotFound from "../../assets/image-404.png"
import "./PokeDetail.scss"

const PokeDetail = () => {
  const { id } = useParams()
  const navigate = useNavigate()

  const DisplayPokemonDetail = () => {
    const { loading, error, data } = usePokeDetail(id) // custom hook
    if (loading) return <p>Loading Pokémon detail...</p>
    if (error) return <p>An error loading Pokémon ocurred!</p>

    const pokemonInfo = data.pokemon_v2_pokemon[0]
    const types = pokemonInfo.pokemon_v2_pokemontypes
    const abilities = pokemonInfo.pokemon_v2_pokemonabilities
    const image =
      JSON.parse(pokemonInfo.pokemon_v2_pokemonsprites[0].sprites).other
        .dream_world.front_default || NotFound

    return data.pokemon_v2_pokemon.map(el => (
      <Box className="poke-info-container" key={el.name}>
        <Box display="flex" flexWrap="wrap">
          <img className="poke-image" alt={el.name} src={image} />
          <Box className="poke-text">
            <Typography variant="h3" marginBottom={3}>
              <span className="poke-number">#{el.id} -</span>{" "}
              <span className="poke-name">{el.name}</span>
            </Typography>
            <Typography textTransform="capitalize" variant="h5">
              Types:{" "}
              {types.map((t, index) => (
                <span key={index}>
                  {(index ? ", " : "") + t.pokemon_v2_type.name}
                </span>
              ))}
            </Typography>
            <Typography textTransform="capitalize" variant="h5">
              Abilities:{" "}
              {abilities.map((a, index) => (
                <span key={a.pokemon_v2_ability.name}>
                  {(index ? ", " : "") + a.pokemon_v2_ability.name}
                </span>
              ))}
            </Typography>
            <Typography
              variant="h5"
              className="general-information-item"
              textTransform="capitalize">
              <span>Base experience: {el.base_experience}</span>
              <span>Weight: {el.weight}</span>
              <span>Height: {el.height}</span>
              <span>
                Habitat: {data.pokemon_v2_pokemonhabitat_by_pk?.name || "-"}
              </span>
              <span>
                Shape: {data.pokemon_v2_pokemonshape_by_pk?.name || "-"}
              </span>
              <span>Region: {data.pokemon_v2_region_by_pk?.name || "-"}</span>
              <span>
                Capture rate: {pokemonInfo.pokemon_v2_pokemonspecy.capture_rate}
              </span>
              <span>
                Base happiness:{" "}
                {pokemonInfo.pokemon_v2_pokemonspecy?.base_happiness}
              </span>
            </Typography>
          </Box>
        </Box>
        <Box paddingLeft={2} paddingRight={2}>
          <Typography variant="h5" marginBottom={2}>
            Stats
          </Typography>
          {pokemonInfo.pokemon_v2_pokemonstats.map(stat => (
            <StatBar label={stat.pokemon_v2_stat.name} value={stat.base_stat} />
          ))}
        </Box>
      </Box>
    ))
  }

  return (
    <Box className="detail-container">
      <DisplayPokemonDetail />
      <Button className="back-btn" onClick={() => navigate(-1)}>
        Go Back
      </Button>
    </Box>
  )
}

export default PokeDetail
