import { useState, useEffect, useRef } from "react"
import { Box, Pagination } from "@mui/material"
import Grid from "@mui/material/Unstable_Grid2"
import SearchBar from "../../components/SearchBar/SearchBar"
import "./PokeList.scss"
import PokeCard from "../../components/Card/PokeCard"
import axios from "axios"

const PokeList = () => {
  const effectRan = useRef(false)
  const [page, setPage] = useState(1)
  const [pageSize, setPageSize] = useState(60)
  const [pokelistFull, setPokelistFull] = useState([])
  const [pokelistVisible, setPokelistVisible] = useState([])
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)
  const [pokemonName, setPokemonName] = useState("")

  useEffect(() => {
    // The new React 18 strict mode runs useEffect twice, the following quick-fix prevents that in order to avoid using more libraries.
    if (effectRan.current === true || process.env.NODE_ENV !== "development") {
      getPokemon() // gets only the pokemon list based on the current page number
    }
    return () => {
      effectRan.current = true
    }
  }, [page, pageSize])

  useEffect(() => {
    if (effectRan.current === true || process.env.NODE_ENV !== "development") {
      getAllPokemon() // fills the SearchBar options with all pokemon
    }
    return () => {
      effectRan.current = true
    }
  }, [])

  const getAllPokemon = () => {
    // get all pokemon list and store in a state to pass it as a prop to the SearchBar component
    const url = `https://pokeapi.co/api/v2/pokemon?offset=0&limit=5000`
    axios
      .get(url)
      .then(res => {
        setPokelistFull(res.data.results)
      })
      .catch(error => {
        setLoading(false)
        setError(error)
      })
  }

  const getPokemon = () => {
    // gets only the pokemon list based on the current page number
    setLoading(true)
    const offset = page === 1 ? 0 : pageSize * (page - 1)
    const url = `https://pokeapi.co/api/v2/pokemon?offset=${offset}&limit=${pageSize}`
    axios
      .get(url)
      .then(res =>
        Promise.all(res.data.results.map(pokemon => axios.get(pokemon.url)))
      )
      .then(arr => {
        setLoading(false)
        setPokelistVisible(arr.map(response => response.data))
      })
      .catch(error => {
        setLoading(false)
        setError(error)
      })
  }

  const searchPokemon = result => {
    // get the pokemon that the user searched for
    setLoading(true)
    if (result) {
      const arr = []
      axios
        .get(result.url)
        .then(res => {
          setLoading(false)
          arr.push(res)
          setPokelistVisible(arr.map(el => el.data))
        })
        .catch(error => {
          setLoading(false)
          setError(error)
        })
    } else {
      getPokemon()
    }
  }

  const sortPokemon = sortValue => {
    const sortedArr = [...pokelistVisible]
    switch (sortValue) {
      case "id-asc":
        sortedArr.sort((a, b) => a.id - b.id)
        break
      case "id-desc":
        sortedArr.sort((a, b) => b.id - a.id)
        break
      case "name-asc":
        sortedArr.sort((a, b) => (a.name > b.name ? 1 : -1))
        break
      case "name-desc":
        sortedArr.sort((a, b) => (b.name > a.name ? 1 : -1))
        break
      default:
        break
    }
    setPokelistVisible(sortedArr)
  }

  return (
    <Box className="list-container" display="flex" flexDirection="column">
      {!loading && !error && (
        <>
          <SearchBar
            pokelist={pokelistFull}
            getSearchResult={searchPokemon}
            getSortValue={sortPokemon}
            getPageSize={size => setPageSize(size)}
            pageSize={pageSize}
            setPageSize={setPageSize}
            pokemonName={pokemonName}
            setPokemonName={setPokemonName}
          />
          <Grid container spacing={5} justifyContent="center" marginTop={1}>
            {pokelistVisible.map(pok => {
              return (
                <Grid item key={pok.name}>
                  <PokeCard pokemon={pok} />
                </Grid>
              )
            })}
          </Grid>
          <Pagination
            style={{ marginTop: "1em" }}
            size="medium"
            page={page}
            count={Math.ceil(pokelistFull.length / pageSize)}
            onChange={(event, value) => setPage(value)}
            color="primary"
          />
        </>
      )}
      {error && (
        <>
          <p>An error ocurred, please refresh the page.</p>
          <p>If the problem persists, contact the administrator.</p>
        </>
      )}
      {loading && <p>Loading Pokemon list...</p>}
    </Box>
  )
}

export default PokeList
